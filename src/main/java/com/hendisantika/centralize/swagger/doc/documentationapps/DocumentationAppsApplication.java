package com.hendisantika.centralize.swagger.doc.documentationapps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DocumentationAppsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DocumentationAppsApplication.class, args);
    }
}
